import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/services/http'
import { useLoadingStore } from './loading'
import temperatureService from '@/services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()
  

async function callConvert(){
  console.log('Store: call convert')
//   result.value = convert(celsius.value)
loadingStore.doLoad()
try{
        result.value = await temperatureService.convert(celsius.value)
} catch(e){
    console.log(e)
}
loadingStore.finish()
console.log('Store: finish call convert')

}

  return { valid, result, celsius, callConvert }
})
